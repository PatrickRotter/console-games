# ConsoleGame
First little console game I wrote.

You are the player 'P' and your enemy is 'X'.
When striking the enemy you gain points.
The game's goal is to gain an amount of 150 points.
However, be careful not to lose all of your health.

Movement is done with W,A,S,D and striking with SPACEBAR.

There are two types of pickups: $ - gain points; H - gain health

Enjoy the game.
