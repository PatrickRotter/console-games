﻿// Copyright 2019 by Patrick Rotter

using System;

namespace ConsoleGame
{
    class Game
    {
        static void Main(string[] args)
        {
            int height = 32;                                                    //
            int width = 64;                                                     //Definition of game area (field)
            string[,] field = new string[height, width];                        //

            Random rnd = new Random();

            int playerVertical = 10;                                            //
            int playerHorizontal = 20;                                          //
            int playerHealth = 100;                                             //Definition of player
            int playerPoints = 0;                                               //
            int count = 0;                                                      //

            int enemyVertical = rnd.Next(2, 29);                                //Enemy position
            int enemyHorizontal = rnd.Next(2, 61);                              //

            int pickUpVertical1 = rnd.Next(2, 29);                              //
            int pickUpHorizontal1 = rnd.Next(2, 61);                            //
            int pickUpVertical2 = rnd.Next(2, 29);                              //Position of obstacles
            int pickUpHorizontal2 = rnd.Next(2, 61);                            //
            int pickUpVertical3 = rnd.Next(2, 29);                              //
            int pickUpHorizontal3 = rnd.Next(2, 61);                            //

            while (playerHealth > 0 && playerPoints < 150)                      //GameLoop
            {
                FieldUpdate();
                PlayerControls();
                EnemyAttack();
                RandomPosition();
                PickUpEvent();
            }

            if (playerHealth <= 0)                                              //Winning / Losing messages
            {
                Console.WriteLine("\nYou lose...");
            }
            else
            {
                Console.WriteLine("You win with total points of {0}!", playerPoints);
            }

            void FieldCreation()
            {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                        {
                            field[i, j] = "*";
                        }
                        else
                        {
                            if (i == playerVertical && j == playerHorizontal)
                            {
                                field[i, j] = "P";
                            }
                            else if (i == enemyVertical && j == enemyHorizontal)
                            {
                                field[i, j] = "X";
                            }
                            else if (i == pickUpVertical1 && j == pickUpHorizontal1)
                            {
                                field[i, j] = "$";
                            }
                            else if (i == pickUpVertical2 && j == pickUpHorizontal2)
                            {
                                field[i, j] = "$";
                            }
                            else if (i == pickUpVertical3 && j == pickUpHorizontal3)
                            {
                                field[i, j] = "H";
                            }
                            else
                            {
                                field[i, j] = " ";
                            }
                        }
                    }
                }
            }

            void FieldUpdate()
            {
                FieldCreation();
                Console.Clear();
                Console.WriteLine("Health: " + playerHealth.ToString() + "            Points: " + playerPoints.ToString());

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Console.Write(field[y, x]);
                    }
                    Console.WriteLine("");
                }
            }

            void PlayerControls()
            {
                string input = Console.ReadKey().Key.ToString();
                switch (input)
                {
                    case "W":
                        if (playerVertical == 1)
                        {
                            playerVertical = height - 2;
                        }
                        else
                        {
                            playerVertical--;
                        }
                        count++;
                        break;

                    case "A":
                        if (playerHorizontal == 1)
                        {
                            playerHorizontal = width - 2;
                        }
                        else
                        {
                            playerHorizontal--;
                        }
                        count++;
                        break;

                    case "S":
                        if (playerVertical == height - 2)
                        {
                            playerVertical = 1;
                        }
                        else
                        {
                            playerVertical++;
                        }
                        count++;
                        break;

                    case "D":
                        if (playerHorizontal == width - 2)
                        {
                            playerHorizontal = 1;
                        }
                        else
                        {
                            playerHorizontal++;
                        }
                        count++;
                        break;

                    case "Spacebar":                                //Attack
                        if ((playerHorizontal == enemyHorizontal && playerVertical - 1 == enemyVertical)
                            || (playerHorizontal == enemyHorizontal && playerVertical + 1 == enemyVertical)
                            || (playerVertical == enemyVertical && playerHorizontal - 1 == enemyHorizontal)
                            || (playerVertical == enemyVertical && playerHorizontal + 1 == enemyHorizontal))
                        {
                            enemyVertical = rnd.Next(2, 29);
                            enemyHorizontal = rnd.Next(2, 61);

                            playerPoints += 10;
                            playerHealth -= 5;
                            Console.Beep();
                            Console.Beep();
                        }
                        break;

                    default:
                        break;
                }
            }

            void EnemyAttack()
            {
                if (playerVertical == enemyVertical && playerHorizontal == enemyHorizontal)      //Enemy attack
                {
                    playerVertical = rnd.Next(2, 29);
                    playerHorizontal = rnd.Next(2, 61);
                    playerHealth -= 20;
                    Console.Beep(400, 800);
                }
            }

            void RandomPosition()
            {
                if (count % 40 == 0)                                 //Enemy and obstacle random positon every n-th turn
                {
                    enemyVertical = rnd.Next(2, 29);
                    enemyHorizontal = rnd.Next(2, 61);
                    pickUpVertical1 = rnd.Next(2, 29);
                    pickUpHorizontal1 = rnd.Next(2, 61);
                    pickUpVertical2 = rnd.Next(2, 29);
                    pickUpHorizontal2 = rnd.Next(2, 61);
                    pickUpVertical3 = rnd.Next(2, 29);
                    pickUpHorizontal3 = rnd.Next(2, 61);
                }
            }

            void PickUpEvent()
            {
                if (playerVertical == pickUpVertical1 && playerHorizontal == pickUpHorizontal1)
                {
                    playerPoints += 5;
                    Console.Beep(2800, 500);
                    Console.Beep(2800, 500);
                    pickUpVertical1 = rnd.Next(2, 29);
                    pickUpHorizontal1 = rnd.Next(2, 61);
                }
                else if (playerVertical == pickUpVertical2 && playerHorizontal == pickUpHorizontal2)
                {
                    playerPoints += 5;
                    Console.Beep(2800, 500);
                    Console.Beep(2800, 500);
                    pickUpVertical2 = rnd.Next(2, 29);
                    pickUpHorizontal2 = rnd.Next(2, 61);
                }
                else if (playerVertical == pickUpVertical3 && playerHorizontal == pickUpHorizontal3)
                {
                    if (playerHealth > 90)
                    {
                        playerHealth = 100;
                    }
                    else
                    {
                        playerHealth += 10;
                    }

                    Console.Beep(2800, 500);
                    Console.Beep(2800, 500);
                    pickUpVertical3 = rnd.Next(2, 29);
                    pickUpHorizontal3 = rnd.Next(2, 61);
                }
            }
        }
    }
}
